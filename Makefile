TARGET = gofetch
PREFIX = /usr/local/bin
CC = go

$(TARGET): $(TARGET).go
	$(CC) build $(TARGET).go

clean:
	rm -f $(TARGET)

install: $(TARGET)
	mkdir -p $(PREFIX)
	cp -f $(TARGET) $(PREFIX)
	chmod 755 $(PREFIX)/$(TARGET)

uninstall:
	rm -f $(PREFIX)/$(TARGET)
