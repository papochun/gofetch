package main
import (
	"fmt"
	"os"
	"bufio"
	"strings"
	"strconv"
)

func flw(input_file string, line int, word int) string {
	file, err := os.Open(input_file)
	if err != nil {
		return "file could not be opened"
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	for i:=1; scanner.Scan(); i++ {
		if i == line {
			buffer := strings.Fields(scanner.Text())
			return buffer[word-1]
		}
	}
	return "flw out of range"
}

func contains_line(input_file string, substring string) string {
	file, err := os.Open(input_file)
	if err != nil {
		return "file could not be opened"
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	for i:=1; scanner.Scan(); i++ {
		if strings.Contains(scanner.Text(), substring) {
			return scanner.Text()
		}
	}
	return "flw out of range"
}

func get_user_at_host() string {
	user := os.Getenv("USER")
	host := flw("/proc/sys/kernel/hostname",1,1)
	return fmt.Sprintf("%s@%s", user, host)
}

func get_os() string {
	os := contains_line("/etc/os-release","NAME=")
	os = strings.TrimLeft(os,"NAME=\"")
	return strings.TrimRight(os,"\"")
}

func get_load() string {
	return flw("/proc/loadavg", 1, 1)
}

func get_kernel() string {
	return flw("/proc/version",1,3)
}

func get_uptime() string {
	uptime, _ := strconv.ParseFloat(flw("/proc/uptime", 1, 1),32)
	hour := uptime / 3600
	min := 60 * (hour - float64(int(hour)))
	return fmt.Sprintf("%dH %dM", int(hour), int(min))
}

func get_ram() int {
	total , _ := strconv.Atoi(flw("/proc/meminfo", 1, 2))
	free  , _ := strconv.Atoi(flw("/proc/meminfo", 2, 2))
	buffer, _ := strconv.Atoi(flw("/proc/meminfo", 4, 2))
	cached, _ := strconv.Atoi(flw("/proc/meminfo", 5, 2))
	slab  , _ := strconv.Atoi(flw("/proc/meminfo", 24, 2))
	return (total - free - buffer - cached - slab) / 1000
}

func get_shell() string {
	return os.Getenv("SHELL")
}

func main() {
	fmt.Printf("%s\n", get_user_at_host())
	fmt.Printf("OS     & %s\n", get_os())
	fmt.Printf("LOAD   & %s\n", get_load())
	fmt.Printf("KERNEL & %s\n", get_kernel())
	fmt.Printf("UPTIME & %s\n", get_uptime())
	fmt.Printf("MEMORY & %dMb\n", get_ram())
	fmt.Printf("SHELL  & %s\n", get_shell())
}
